package com.virginvoyages.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.virginvoyages.model.User;
import com.virginvoyages.service.UserService;


@RestController
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping("/")
	public ModelAndView landingPage() {
		return new ModelAndView("welcome");
	}
	
	@RequestMapping("/welcome")
	public ModelAndView firstPage() {
		return new ModelAndView("welcome");
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.GET)
	public ModelAndView show() {
		return new ModelAndView("addUser", "usr", new User());
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public ModelAndView processRequest(@ModelAttribute("usr") User usr) {
		
		userService.insertUser(usr);
		List<User> users = userService.getAllUsers();
		ModelAndView model = new ModelAndView("getUsers");
		model.addObject("users", users);
		return model;
	}

	@RequestMapping("/getUsers")
	public ModelAndView getEmployees() {
		List<User> users = userService.getAllUsers();
		ModelAndView model = new ModelAndView("getUsers");
		model.addObject("users", users);
		return model;
	}

}
