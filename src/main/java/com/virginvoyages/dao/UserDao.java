package com.virginvoyages.dao;

import java.util.List;

import com.virginvoyages.model.User;

public interface UserDao {
	void insertUser(User cus);
	void insertUsers(List<User> employees);
	List<User> getAllUsers();
	User getUserById(String empId);
}