package com.virginvoyages.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.virginvoyages.dao.UserDao;
import com.virginvoyages.model.User;

@Repository
public class UserDaoImpl extends JdbcDaoSupport implements UserDao{
	
	@Autowired 
	DataSource dataSource;
	
	@PostConstruct
	private void initialize(){
		setDataSource(dataSource);
	}
	
	@Override
	public void insertUser(User usr) {
		String sql = "INSERT INTO users " +
				"(userId, userName ) VALUES (?, ?)" ;
		getJdbcTemplate().update(sql, new Object[]{
				usr.getUserId(), usr.getUserName()
		});
	}
	
	@Override
	public void insertUsers(final List<User> user) {
		String sql = "INSERT INTO users " + "(userId, userName) VALUES (?, ?)";
		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				User usr = user.get(i);
				ps.setString(1, usr.getUserId());
				ps.setString(2, usr.getUserName());
			}
			
			public int getBatchSize() {
				return user.size();
			}
		});

	}
	@Override
	public List<User> getAllUsers(){
		String sql = "SELECT * FROM users";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
		
		List<User> result = new ArrayList<User>();
		for(Map<String, Object> row:rows){
			User usr = new User();
			usr.setUserId((String)row.get("userId"));
			usr.setUserName((String)row.get("userName"));
			result.add(usr);
		}
		
		return result;
	}

	@Override
	public User getUserById(String empId) {
		String sql = "SELECT * FROM users WHERE userId = ?";
		return (User)getJdbcTemplate().queryForObject(sql, new Object[]{empId}, new RowMapper<User>(){
			@Override
			public User mapRow(ResultSet rs, int rwNumber) throws SQLException {
				User usr = new User();
				usr.setUserId(rs.getString("userId"));
				usr.setUserName(rs.getString("userName"));
				return usr;
			}
		});
	}
}