package com.virginvoyages.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virginvoyages.dao.UserDao;
import com.virginvoyages.model.User;
import com.virginvoyages.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;

	@Override
	public void insertUser(User usr) {
		userDao.insertUser(usr);
	}

	@Override
	public void insertUsers(List<User> usr) {
		userDao.insertUsers(usr);
	}

	public List<User> getAllUsers() {
		return userDao.getAllUsers();
	}

	@Override
	public void getUserById(String userId) {
		User usrDao = userDao.getUserById(userId);
		System.out.println(usrDao);
	}

}