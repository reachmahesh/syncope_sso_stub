package com.virginvoyages.service;

import java.util.List;

import com.virginvoyages.model.User;

public interface UserService {
	void insertUser(User usr);
	void insertUsers(List<User> users);
	List<User> getAllUsers();
	void getUserById(String userid);
}